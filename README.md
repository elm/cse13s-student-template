All of your work for CSE 13S _must_ go into this repository.
The work for an individual assignment must be stored _only_ in the directory for that assignment.
For example, all of the files for Assignment 1 must go into the `asgn1` directory.

Your repository will be checked to ensure that it meets minimum requirements for submission each
time you push changes. If the repository meets the requirements for the current assignment,
you'll see a green badge with a check mark at the top of the home page for the repo.
If it doesn't meet minimum requirements, you'll see a red badge with an X.

The repository check takes time, anywhere from a few seconds to minutes when the system is
busy, _**as it will be right before a due date**_.
You're _strongly_ encouraged to make commits and push them well before an assignment is due;
slow system response from hundreds of CSE 13S students all procrastinating
is _not_ a valid excuse for submitting your work late.

You may submit a commit ID even if your the repository check from the push hasn't completed.
However, doing so runs the risk that, if you made a mistake, you'll be submitting a commit
ID that doesn't meet minimum requirements.
